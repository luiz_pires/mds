import { Api } from './api/api';
import { Items } from '../mocks/providers/items';
import { TurmaProvider } from './turma/turma';
import { AlunoProvider } from './aluno/aluno';
import { MatriculaProvider } from './matricula/matricula';
import { DisciplinaProvider } from './disciplina/disciplina';
import { HistoricoProvider } from './historico/historico';
import { Settings } from './settings/settings';
import { User } from './user/user';
import { Global } from './global/global';

export {
    Api,
    MatriculaProvider,
    TurmaProvider,
    AlunoProvider,
    DisciplinaProvider,
    HistoricoProvider,
    Global,
    Items,
    Settings,
    User
};
