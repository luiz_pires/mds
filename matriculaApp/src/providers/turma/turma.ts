import { Injectable } from '@angular/core';
import { Api} from '../api/api';
import { Global } from '../global/global';
import { Turma } from '../../models/turma';

@Injectable()
export class TurmaProvider {
  listaTurma: Turma[] = [];
  periodoLetivo;

  basePath : string = "/entidade/turma"

  constructor(public api: Api, public global: Global) {
    this.periodoLetivo = this.global.getPeriodoLetivo()
  }

  pesquisar(params?: any) {
    var path : string = "/" + this.periodoLetivo.ano + "/" + this.periodoLetivo.numero;
    return this.api.get(this.basePath + path,params);
    // if (!params) {
    //   return this.listaTurma;
    // }
    // if ("disciplina" in params) {
    //   return this.listaTurma.filter((turma) => {
    //     if(turma.disciplina.codigo == params.disciplina) {
    //           return turma;
    //     }
    //     return null;
    //   });
    // }
  }

}
