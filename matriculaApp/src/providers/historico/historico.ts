import { Injectable } from '@angular/core';
import { Api } from '../api/api';

@Injectable()
export class HistoricoProvider {
	basePath = "entidade/historicoescolar";

	constructor(public api: Api){

	}

	consultarHistorico(aluno: string){
		return this.api.get(this.basePath + "/" + aluno);
	}

	consultarIRA(aluno: string){
		return this.api.get(this.basePath + "/" + aluno + '/ira');
	}

	pesquisarDisciplina(aluno: string, disciplina:string){	
		return this.api.get(this.basePath + "/" + aluno + "/" + disciplina);
	}

	pesquisarPreRequisitos(aluno: string, disciplina:string){
		return this.api.get(this.basePath + "/" + aluno + "/" + disciplina + "/prerequisito");
	}
}