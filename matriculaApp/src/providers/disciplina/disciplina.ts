import { Injectable } from '@angular/core';
import { Api} from '../api/api';
import { Disciplina } from '../../models/disciplina';

@Injectable()
export class DisciplinaProvider {
  listaDisciplina: Disciplina[] = [];
  basePath = "entidade/disciplina";

  constructor(public api: Api) {  
  }

  pesquisar(params?: any) {

    return this.api.get(this.basePath,params);
  }


  consultar(disciplina: string){
  	var path: string = '/' + disciplina;
  	return this.api.get(this.basePath + path);
  }

}
