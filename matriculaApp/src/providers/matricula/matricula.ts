import { Injectable } from '@angular/core';

import { Matricula } from '../../models/matricula';
import { MatriculaAluno } from '../../models/matriculaAluno';
import { Aluno } from '../../models/aluno';
import { Turma } from '../../models/turma';
import { Api } from '../api/api';
import { Global } from '../global/global';
import { AlunoProvider } from '../providers';

import {Observable} from 'rxjs'
//import { forkJoin } from 'rxjs/observable/forkJoin';

@Injectable()
export class MatriculaProvider {

  basePath : string;
  periodoLetivo: string;
  listaMatriculaAluno;
  //listaMatriculaAluno: MatriculaAluno[] = [];
  listaMatriculaHistorico;

  constructor(public api: Api, public global: Global, public alunoProvider: AlunoProvider) {
    this.periodoLetivo = global.getPeriodoLetivo().ano+'/'+global.getPeriodoLetivo().numero;
    this.updateBasePath();
  }

  updateBasePath() {
    this.basePath = 'entidade/matricula';
    //if(this.global.getSufixoGrupo().length>1)
      //this.basePath += '_'+this.global.getSufixoGrupo();
  }

  /* Cached */
  getMatriculaAluno() {
      return this.listaMatriculaAluno;
  }

  getMatriculaHistorico() {
      return this.listaMatriculaHistorico;
  }

  setMatriculaAluno(matriculaAluno) {
  //setMatriculaAluno(matriculaAluno: MatriculaAluno[]) {
      this.listaMatriculaAluno = matriculaAluno;
  }

  setMatriculaHistorico(matriculaHistorico) {
    this.listaMatriculaHistorico = matriculaHistorico;
  }

  resetMatriculaAluno() {
    this.listaMatriculaAluno = [];
    this.listaMatriculaHistorico = [];
  }

  add(matriculaAluno: MatriculaAluno){
    this.listaMatriculaAluno.push(matriculaAluno);
  }

  delete(matriculaAluno: MatriculaAluno) {
    this.listaMatriculaAluno.splice(this.listaMatriculaAluno.indexOf(matriculaAluno), 1);
  }

  updateStatus(matriculaAluno: MatriculaAluno, status: string) {
    if(!matriculaAluno.statusAnterior) {
      matriculaAluno.statusAnterior = matriculaAluno.status;
    }
    matriculaAluno.status = status;
  }

  /* Service */
  pesquisarAluno(aluno: string, params? : any) {
    var path = '/'+this.periodoLetivo+'/aluno/'+aluno;
    return this.api.get(this.basePath+path, params);
  }

  pesquisarHistorico(aluno: string) {
    var path = '/'+this.periodoLetivo+'/aluno/'+aluno+'/historico';
    return this.api.get(this.basePath+path);
  }


  pesquisarConflito(aluno:string, turma : Turma){
    var path = '/' + this.periodoLetivo + '/aluno/' + aluno+ '/' + turma.disciplina.codigo + '/' + turma.codigo +  '/conflito';
    return this.api.get(this.basePath+path);
  }

  inserir(matricula: Matricula) {
    var path = '/';
    return this.api.post(this.basePath+path,matricula);
  }

  alterarStatus(matricula: Matricula, statusAnterior: string) {
    var path = '/';
    var alterarStatusRequest = {
      matricula: matricula,
	    statusAnterior: statusAnterior
    }
    return this.api.put(this.basePath+path,alterarStatusRequest);
  }

  /* Tasks */
  confirmarMatricula() {
    let observables : Observable<any>[] = [];
    let aluno : Aluno = this.global.getAluno();

    for(let matriculaAluno of this.listaMatriculaAluno) {
      if(
      ((matriculaAluno.statusAnterior) && (matriculaAluno.status !== matriculaAluno.statusAnterior))|| // alteracao de status
         (!matriculaAluno.status)) { // novo pedido

        var matricula : Matricula = {
          turma: {
            codigo: matriculaAluno.turma.codigo,
            disciplina: matriculaAluno.turma.disciplina.codigo,
            periodoLetivo: this.global.getPeriodoLetivo()
          },
          aluno: aluno.matricula
        }

        if(!matriculaAluno.status) {
          matricula.status = 'Pedido';
          if(matriculaAluno.prioridade) {
            matricula.prioridade = matriculaAluno.prioridade;
          }
          observables.push(this.inserir(matricula));
        } else {
          matricula.status = matriculaAluno.status;
          observables.push(this.alterarStatus(matricula,matriculaAluno.statusAnterior));
        }
      }
    }

    return Observable.forkJoin(observables);
  }

  ajustarMatricula(matriculaAluno : MatriculaAluno, statusAnterior? : string) {
    let aluno : Aluno = this.global.getAluno();
    var matricula : Matricula = {
      aluno: aluno.matricula,
      turma: {
        codigo: matriculaAluno.turma.codigo,
        disciplina: matriculaAluno.turma.disciplina.codigo,
        periodoLetivo: this.global.getPeriodoLetivo()
      },
      status : matriculaAluno.status
    };
    if(!statusAnterior)
      return this.inserir(matricula);
    else
      return this.alterarStatus(matricula,statusAnterior);
  }

}
