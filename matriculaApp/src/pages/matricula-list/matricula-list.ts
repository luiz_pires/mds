import { Component } from '@angular/core';
import { IonicPage, NavController,  AlertController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { MatriculaProvider } from '../../providers/providers';
import { Global } from '../../providers/providers';
import { Aluno } from '../../models/aluno';
import { MatriculaAluno } from '../../models/matriculaAluno';

export interface AlertMetadata {
  title: string;
  description: string;
  okButton: string;
  cancelButton: string;
}


@IonicPage()
@Component({
  selector: 'page-matricula-list',
  templateUrl: 'matricula-list.html'
})
export class MatriculaListPage {
  currentListaMatricula;
  alertaConfirmaMatricula: AlertMetadata;
  alertaPreMatricula: AlertMetadata;
  atualizarPagina:boolean = true;
  currentAluno: Aluno;
  faseMatricula: string;
  send:boolean =false;

  constructor(public navCtrl: NavController,
    public matriculaProvider: MatriculaProvider,
    public global: Global,
    private alertCtrl: AlertController,
    translate: TranslateService) {

    translate.get(["CONFIRMAR_MATRICULA_TITLE",
      "CONFIRMAR_MATRICULA_DESCRIPTION",
      "CONFIRM_BUTTON",
      "CANCEL_BUTTON",
      "AVISO_MATRICULA_TITLE",
      "AVISO_MATRICULA_DESCRIPTION"
      ]).subscribe(
      (values) => {
        this.alertaConfirmaMatricula = {
          title : values.CONFIRMAR_MATRICULA_TITLE,
          description: values.CONFIRMAR_MATRICULA_DESCRIPTION,
          okButton: values.CONFIRM_BUTTON,
          cancelButton: values.CANCEL_BUTTON
        };

        this.alertaPreMatricula = {
          title : values.AVISO_MATRICULA_TITLE,
          description: values.AVISO_MATRICULA_DESCRIPTION,
          okButton: 'OK',
          cancelButton: ''
        };
      })
}

  /**
   * The view loaded and is about to enter, let's query our items for the list
   */
  ionViewWillEnter() {
    if(this.atualizarPagina){
      this.currentAluno = this.global.getAluno();
      if(this.currentAluno){
        this.matriculaProvider.pesquisarAluno(this.global.getAluno().matricula)
          .subscribe(listaMatricula => {
        this.currentListaMatricula = listaMatricula;
        this.matriculaProvider.listaMatriculaAluno = listaMatricula;
        });
      }    
      this.faseMatricula = this.global.getFaseMatricula();
      this.atualizarPagina = true;
    }
  }

  /**
   * Prompt the user to add a new item. This shows our MatriculaCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
/*
  addItem() {
    let addModal = this.modalCtrl.create('MatriculaCreatePage');
    addModal.onDidDismiss(matriculaAluno => {
      if (matriculaAluno) {
        this.matriculaProvider.add(matriculaAluno);
      }
    })
    addModal.present();
  }
*/

  /**
   * Delete an item from the list of items.
   */
  deleteItem(matricula) {
    this.matriculaProvider.delete(matricula);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(matricula: MatriculaAluno) {
    this.atualizarPagina = false;
    this.navCtrl.push('MatriculaDetailPage', {
      matricula: matricula
    });
  }

  confirmarMatricula() {
    this.matriculaProvider.confirmarMatricula().subscribe(data => {
      this.matriculaProvider.pesquisarAluno(this.global.getAluno().matricula).subscribe(listaMatricula => {
          this.currentListaMatricula = listaMatricula;
            this.matriculaProvider.listaMatriculaAluno = this.currentListaMatricula;
      })


    })

}

  confirmarConfirmacaoMatricula() {
      const confirm = this.alertCtrl.create({
      title: this.alertaConfirmaMatricula.title,
      message: this.alertaConfirmaMatricula.description,
      buttons: [
        {
          text: this.alertaConfirmaMatricula.cancelButton,
        },
        {
          text: this.alertaConfirmaMatricula.okButton,
          handler: () => {
            this.confirmarMatricula();
          }
        }
      ]
    });
    confirm.present();
  }

  addItem(){
    this.atualizarPagina = false;
    if(this.global.getPerfil() == 'Coordenador'){
      this.navCtrl.parent.select(2);
    }
    else{
      this.navCtrl.parent.select(1);
    }
  }


  verificarConfirmacaoMatricula() {
    for (let matriculaAluno of this.currentListaMatricula){
      if(matriculaAluno.status == 'PreMatricula'){
        const warning = this.alertCtrl.create({
        title: this.alertaPreMatricula.title,
        message: this.alertaPreMatricula.description,
        buttons: [
          {
            text: this.alertaPreMatricula.okButton,
          },
        ]});

        warning.present();
        return;
      }
    }
      this.confirmarConfirmacaoMatricula();
  }

}
