import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams } from 'ionic-angular';

import { Disciplina } from '../../models/disciplina';
import { DisciplinaProvider } from '../../providers/providers';
import { MatriculaProvider } from '../../providers/providers';
import { Global } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-turma-search',
  templateUrl: 'turma-search.html'
})
export class TurmaSearchPage {

  currentListaDisciplina;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public disciplinaProvider: DisciplinaProvider,
    public matriculaProvider: MatriculaProvider,
    public global: Global) {
  }


  /**
   * Perform a service for the proper items.
   */
  getItems(ev) {
    let val = ev.target.value;
    this.currentListaDisciplina = [];
    if (!val || !val.trim()) {
      return;
    }

  val = val.trim().toUpperCase();

  if(val.length > 3){
    if(val.length == 6 && !isNaN(Number(val)))  {
      this.disciplinaProvider.consultar(val)
      .subscribe(disciplina => {
        this.currentListaDisciplina.push(disciplina);
      });
    }
    else{    
    this.disciplinaProvider.pesquisar({
      nome: val
    })
    .subscribe(listaDisciplina => {
      this.currentListaDisciplina = listaDisciplina;
    });
    }
  }
 }    

  /**
   * Navigate to the detail page for this item.
   */
  openItem(disciplina: Disciplina) {
    this.navCtrl.push('TurmaListPage', {
      disciplina: disciplina,
    });
  }

}
