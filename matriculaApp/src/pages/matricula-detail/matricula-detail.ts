import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { MatriculaAluno } from '../../models/matriculaAluno';
import { Global } from '../../providers/providers';
import { MatriculaProvider} from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-matricula-detail',
  templateUrl: 'matricula-detail.html'
})
export class MatriculaDetailPage {
  matricula: MatriculaAluno;
  perfil: string;
  faseMatricula: string;

  constructor(public navCtrl: NavController,
    public matriculaProvider: MatriculaProvider,
    navParams: NavParams,
    public global: Global) {

    this.matricula = navParams.get('matricula');
    this.perfil = global.getPerfil();
    this.faseMatricula = global.getFaseMatricula();
  }

  /**
   * Alterar o Status da matrícula na turma.
   */
  alterarStatus(status: string) {
    this.matriculaProvider.updateStatus(this.matricula, status);
    this.navCtrl.pop();
  }

  /**
   * Retirar matrícula (Coordenador).
   */
  retirarMatricula() {
    var statusAnterior = this.matricula.status;
    this.matricula.status = 'Retirado';

    this.matriculaProvider.alterarStatus({
        status: this.matricula.status,
        aluno: this.global.getAluno().matricula,
        turma: {
          codigo: this.matricula.turma.codigo,
          disciplina: this.matricula.turma.disciplina.codigo,
          periodoLetivo: this.global.getPeriodoLetivo()
      },
      prioridade: this.matricula.prioridade
    },
      statusAnterior).subscribe(
        () =>{
        this.navCtrl.pop();}
      );

  }

}
